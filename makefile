help:
	grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

install: ## Download the carthage dependencies
	echo "Download the carthage dependencies"
	carthage update --platform ios

test: ## Run all tests using iPhone 7 OS = 10.2
	echo "Running tests using iPhone 7 OS = 10.2"
	xcodebuild -project Twitch.xcodeproj -scheme Twitch -sdk iphonesimulator -destination 'platform=iOS Simulator,name=iPhone 7,OS=10.3.1' test
