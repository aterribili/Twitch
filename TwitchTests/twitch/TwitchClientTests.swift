import XCTest
@testable import Twitch
import GCDWebServers

class TwitchClientTests: XCTestCase {
    func testShouldValidateSuccessFlow() {
        let webServer = GCDWebServer()!
        webServer.addDefaultHandler(forMethod: "GET", request: GCDWebServerRequest.self, processBlock: {request in
            return GCDWebServerDataResponse(jsonObject: Responses.validGamesJson)
        })
        webServer.start(withPort: 8000, bonjourName: "Twitch Web Server")

        let expectation = self.expectation(description: "Wait for asynchronous test")
        _ = TwitchClient(url: webServer.serverURL.absoluteString).getTopGames(success: { (games, _) in
            XCTAssertEqual(games.count, 1)
            expectation.fulfill()
        }) { (error) in
            XCTFail("this closure should not be called for this test")
        }
        waitForExpectations(timeout: 3, handler: nil)
    }

    func testShouldValidateFailureFlow() {
        let webServer = GCDWebServer()!
        webServer.addDefaultHandler(forMethod: "GET", request: GCDWebServerRequest.self, processBlock: {request in
            return GCDWebServerDataResponse(statusCode: 404)
        })
        webServer.start(withPort: 8001, bonjourName: "Twitch Web Server")

        let expectation = self.expectation(description: "Wait for asynchronous test")
        _ = TwitchClient(url: webServer.serverURL.absoluteString).getTopGames(success: { (games, _) in
            XCTFail("this closure should not be called for this test")
        }) { (error) in
            XCTAssertNotNil(error)
            expectation.fulfill()
        }
        waitForExpectations(timeout: 3, handler: nil)
    }

    func testShouldValidateSuccessFlowForGetDict() {
        let webServer = GCDWebServer()!
        webServer.addDefaultHandler(forMethod: "GET", request: GCDWebServerRequest.self, processBlock: {request in
            return GCDWebServerDataResponse(jsonObject: Responses.validGamesJson)
        })
        webServer.start(withPort: 8002, bonjourName: "Twitch Web Server")

        let expectation = self.expectation(description: "Wait for asynchronous test")
        _ = TwitchClient(url: webServer.serverURL.absoluteString).getDict(success: { (dict) in
            XCTAssertNotNil(dict)
            expectation.fulfill()
        }) { (error) in
            XCTFail("this closure should not be called for this test")
        }
        waitForExpectations(timeout: 3, handler: nil)
    }

    func testShouldValidateFailureFlowForGetDict() {
        let webServer = GCDWebServer()!
        webServer.addDefaultHandler(forMethod: "GET", request: GCDWebServerRequest.self, processBlock: {request in
            return GCDWebServerDataResponse(statusCode: 404)
        })
        webServer.start(withPort: 8003, bonjourName: "Twitch Web Server")

        let expectation = self.expectation(description: "Wait for asynchronous test")
        _ = TwitchClient(url: webServer.serverURL.absoluteString).getDict(success: { (dict) in
            XCTFail("this closure should not be called for this test")
        }) { (error) in
            XCTAssertNotNil(error)
            expectation.fulfill()
        }
        waitForExpectations(timeout: 3, handler: nil)
    }
}
