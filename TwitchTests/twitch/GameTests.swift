import XCTest
@testable import Twitch

class GameTests: XCTestCase {
    let target = Game(dict: Responses.validGameJson)

    func testShouldValidateViewers() {
        XCTAssertEqual(target?.viewers, .some(129431))
    }

    func testShouldValidateChannels() {
        XCTAssertEqual(target?.channels, .some(1614))
    }

    func testShouldValidateDetails() {
        let details = target?.details
        XCTAssertEqual(details?.name, .some("League of Legends"))
        XCTAssertEqual(details?.popularity, .some(124415))
        XCTAssertNotNil(details?.box)
        XCTAssertNotNil(details?.logo)
    }

    func testWhenViewersIsAbsentGameShouldBeNil() {
        let target = Game(dict: Responses.jsonWithoutViewers)
        XCTAssertNil(target)
    }

    func testWhenChannelsIsAbsentGameShouldBeNil() {
        let target = Game(dict: Responses.jsonWithoutChannels)
        XCTAssertNil(target)
    }

    func testWhenGameDictionaryIsAbsentGameShouldBeNil() {
        let target = Game(dict: Responses.jsonWithoutGame)
        XCTAssertNil(target)
    }
}
