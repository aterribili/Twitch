import XCTest
@testable import Twitch

class DetailsTests: XCTestCase {
    let target = Game.Details(dict: Responses.detailsJson)

    func testShouldValidateDetails() {
        XCTAssertNotNil(target)
    }

    func testShouldValidateName() {
        XCTAssertEqual(target?.name, .some("League of Legends"))
    }

    func testShouldValidatePopularity() {
        XCTAssertEqual(target?.popularity, .some(124415))
    }

    func testShouldValidateBox() {
        XCTAssertNotNil(target?.box)
    }

    func testShouldValidateLogo() {
        XCTAssertNotNil(target?.logo)
    }

    func testWhenNameIsAbsentDetailShouldBeNil() {
        let target = Game.Details(dict: Responses.detailsJsonWithoutName)
        XCTAssertNil(target)
    }

    func testWhenPopularityIsAbsentDetailsShouldNotBeNil() {
        let target = Game.Details(dict: Responses.detailsWithoutPopularity)
        XCTAssertNotNil(target)
    }

    func testWhenBoxIsAbsentDetailsShouldBeNil() {
        let target = Game.Details(dict: Responses.detailsWithoutBox)
        XCTAssertNil(target)
    }

    func testWhenLogoIsAbsentDetailsShouldBeNil() {
        let target = Game.Details(dict: Responses.detailsWithoutLogo)
        XCTAssertNotNil(target)
    }
}
