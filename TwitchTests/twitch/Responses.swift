struct Responses {
    static let validGamesJson: [String: Any] = [
        "_total": 1,
        "top": [[
            "game": [
                "name": "League of Legends",
                "popularity": 124415,
                "_id": 21779,
                "giantbomb_id": 24024,
                "box": [
                    "large": "https://static-cdn.jtvnw.net/ttv-boxart/League%20of%20Legends-272x380.jpg",
                    "medium": "https://static-cdn.jtvnw.net/ttv-boxart/League%20of%20Legends-136x190.jpg",
                    "small": "https://static-cdn.jtvnw.net/ttv-boxart/League%20of%20Legends-52x72.jpg",
                    "template": "https://static-cdn.jtvnw.net/ttv-boxart/League%20of%20Legends-[width]x[height].jpg"
                ],
                "logo": [
                    "large": "https://static-cdn.jtvnw.net/ttv-logoart/League%20of%20Legends-240x144.jpg",
                    "medium": "https://static-cdn.jtvnw.net/ttv-logoart/League%20of%20Legends-120x72.jpg",
                    "small": "https://static-cdn.jtvnw.net/ttv-logoart/League%20of%20Legends-60x36.jpg",
                    "template": "https://static-cdn.jtvnw.net/ttv-logoart/League%20of%20Legends-[width]x[height].jpg"
                ],
                "localized_name": "League of Legends",
                "locale": ""
            ],
            "viewers": 129431,
            "channels": 1614
            ]]
    ]


    static let validGameJson: [String: Any] = [
        "game": [
            "name": "League of Legends",
            "popularity": 124415,
            "_id": 21779,
            "giantbomb_id": 24024,
            "box": [
                "large": "https://static-cdn.jtvnw.net/ttv-boxart/League%20of%20Legends-272x380.jpg",
                "medium": "https://static-cdn.jtvnw.net/ttv-boxart/League%20of%20Legends-136x190.jpg",
                "small": "https://static-cdn.jtvnw.net/ttv-boxart/League%20of%20Legends-52x72.jpg",
                "template": "https://static-cdn.jtvnw.net/ttv-boxart/League%20of%20Legends-[width]x[height].jpg"
            ],
            "logo": [
                "large": "https://static-cdn.jtvnw.net/ttv-logoart/League%20of%20Legends-240x144.jpg",
                "medium": "https://static-cdn.jtvnw.net/ttv-logoart/League%20of%20Legends-120x72.jpg",
                "small": "https://static-cdn.jtvnw.net/ttv-logoart/League%20of%20Legends-60x36.jpg",
                "template": "https://static-cdn.jtvnw.net/ttv-logoart/League%20of%20Legends-[width]x[height].jpg"
            ],
            "localized_name": "League of Legends",
            "locale": ""
        ],
        "viewers": 129431,
        "channels": 1614
    ]

    static let jsonWithoutViewers: [String: Any] = [
        "game": [
            "name": "League of Legends",
            "popularity": 124415,
            "_id": 21779,
            "giantbomb_id": 24024,
            "box": [
                "large": "https://static-cdn.jtvnw.net/ttv-boxart/League%20of%20Legends-272x380.jpg",
                "medium": "https://static-cdn.jtvnw.net/ttv-boxart/League%20of%20Legends-136x190.jpg",
                "small": "https://static-cdn.jtvnw.net/ttv-boxart/League%20of%20Legends-52x72.jpg",
                "template": "https://static-cdn.jtvnw.net/ttv-boxart/League%20of%20Legends-[width]x[height].jpg"
            ],
            "logo": [
                "large": "https://static-cdn.jtvnw.net/ttv-logoart/League%20of%20Legends-240x144.jpg",
                "medium": "https://static-cdn.jtvnw.net/ttv-logoart/League%20of%20Legends-120x72.jpg",
                "small": "https://static-cdn.jtvnw.net/ttv-logoart/League%20of%20Legends-60x36.jpg",
                "template": "https://static-cdn.jtvnw.net/ttv-logoart/League%20of%20Legends-[width]x[height].jpg"
            ],
            "localized_name": "League of Legends",
            "locale": ""
        ],
        "channels": 1614
    ]

    static let jsonWithoutChannels: [String: Any] = [
        "game": [
            "name": "League of Legends",
            "popularity": 124415,
            "_id": 21779,
            "giantbomb_id": 24024,
            "box": [
                "large": "https://static-cdn.jtvnw.net/ttv-boxart/League%20of%20Legends-272x380.jpg",
                "medium": "https://static-cdn.jtvnw.net/ttv-boxart/League%20of%20Legends-136x190.jpg",
                "small": "https://static-cdn.jtvnw.net/ttv-boxart/League%20of%20Legends-52x72.jpg",
                "template": "https://static-cdn.jtvnw.net/ttv-boxart/League%20of%20Legends-[width]x[height].jpg"
            ],
            "logo": [
                "large": "https://static-cdn.jtvnw.net/ttv-logoart/League%20of%20Legends-240x144.jpg",
                "medium": "https://static-cdn.jtvnw.net/ttv-logoart/League%20of%20Legends-120x72.jpg",
                "small": "https://static-cdn.jtvnw.net/ttv-logoart/League%20of%20Legends-60x36.jpg",
                "template": "https://static-cdn.jtvnw.net/ttv-logoart/League%20of%20Legends-[width]x[height].jpg"
            ],
            "localized_name": "League of Legends",
            "locale": ""
        ],
        "viewers": 129431,
        ]

    static let jsonWithoutGame: [String: Any] = [
        "viewers": 129431,
        "channels": 1614
    ]

    static let detailsJson: [String: Any] = [
        "name": "League of Legends",
        "popularity": 124415,
        "_id": 21779,
        "giantbomb_id": 24024,
        "box": [
            "large": "https://static-cdn.jtvnw.net/ttv-boxart/League%20of%20Legends-272x380.jpg",
            "medium": "https://static-cdn.jtvnw.net/ttv-boxart/League%20of%20Legends-136x190.jpg",
            "small": "https://static-cdn.jtvnw.net/ttv-boxart/League%20of%20Legends-52x72.jpg",
            "template": "https://static-cdn.jtvnw.net/ttv-boxart/League%20of%20Legends-[width]x[height].jpg"
        ],
        "logo": [
            "large": "https://static-cdn.jtvnw.net/ttv-logoart/League%20of%20Legends-240x144.jpg",
            "medium": "https://static-cdn.jtvnw.net/ttv-logoart/League%20of%20Legends-120x72.jpg",
            "small": "https://static-cdn.jtvnw.net/ttv-logoart/League%20of%20Legends-60x36.jpg",
            "template": "https://static-cdn.jtvnw.net/ttv-logoart/League%20of%20Legends-[width]x[height].jpg"
        ],
        "localized_name": "League of Legends",
        "locale": ""
    ]

    static let detailsJsonWithoutName: [String: Any] = [
        "popularity": 124415,
        "_id": 21779,
        "giantbomb_id": 24024,
        "box": [
            "large": "https://static-cdn.jtvnw.net/ttv-boxart/League%20of%20Legends-272x380.jpg",
            "medium": "https://static-cdn.jtvnw.net/ttv-boxart/League%20of%20Legends-136x190.jpg",
            "small": "https://static-cdn.jtvnw.net/ttv-boxart/League%20of%20Legends-52x72.jpg",
            "template": "https://static-cdn.jtvnw.net/ttv-boxart/League%20of%20Legends-[width]x[height].jpg"
        ],
        "logo": [
            "large": "https://static-cdn.jtvnw.net/ttv-logoart/League%20of%20Legends-240x144.jpg",
            "medium": "https://static-cdn.jtvnw.net/ttv-logoart/League%20of%20Legends-120x72.jpg",
            "small": "https://static-cdn.jtvnw.net/ttv-logoart/League%20of%20Legends-60x36.jpg",
            "template": "https://static-cdn.jtvnw.net/ttv-logoart/League%20of%20Legends-[width]x[height].jpg"
        ]
    ]

    static let detailsWithoutPopularity: [String: Any] = [
        "name": "League of Legends",
        "_id": 21779,
        "giantbomb_id": 24024,
        "box": [
            "large": "https://static-cdn.jtvnw.net/ttv-boxart/League%20of%20Legends-272x380.jpg",
            "medium": "https://static-cdn.jtvnw.net/ttv-boxart/League%20of%20Legends-136x190.jpg",
            "small": "https://static-cdn.jtvnw.net/ttv-boxart/League%20of%20Legends-52x72.jpg",
            "template": "https://static-cdn.jtvnw.net/ttv-boxart/League%20of%20Legends-[width]x[height].jpg"
        ],
        "logo": [
            "large": "https://static-cdn.jtvnw.net/ttv-logoart/League%20of%20Legends-240x144.jpg",
            "medium": "https://static-cdn.jtvnw.net/ttv-logoart/League%20of%20Legends-120x72.jpg",
            "small": "https://static-cdn.jtvnw.net/ttv-logoart/League%20of%20Legends-60x36.jpg",
            "template": "https://static-cdn.jtvnw.net/ttv-logoart/League%20of%20Legends-[width]x[height].jpg"
        ],
        "localized_name": "League of Legends",
        "locale": ""
    ]

    static let detailsWithoutBox: [String: Any] = [
        "name": "League of Legends",
        "popularity": 124415,
        "_id": 21779,
        "giantbomb_id": 24024,
        "logo": [
            "large": "https://static-cdn.jtvnw.net/ttv-logoart/League%20of%20Legends-240x144.jpg",
            "medium": "https://static-cdn.jtvnw.net/ttv-logoart/League%20of%20Legends-120x72.jpg",
            "small": "https://static-cdn.jtvnw.net/ttv-logoart/League%20of%20Legends-60x36.jpg",
            "template": "https://static-cdn.jtvnw.net/ttv-logoart/League%20of%20Legends-[width]x[height].jpg"
        ],
        "localized_name": "League of Legends",
        "locale": ""
    ]

    static let detailsWithoutLogo: [String: Any] = [
        "name": "League of Legends",
        "popularity": 124415,
        "_id": 21779,
        "giantbomb_id": 24024,
        "box": [
            "large": "https://static-cdn.jtvnw.net/ttv-boxart/League%20of%20Legends-272x380.jpg",
            "medium": "https://static-cdn.jtvnw.net/ttv-boxart/League%20of%20Legends-136x190.jpg",
            "small": "https://static-cdn.jtvnw.net/ttv-boxart/League%20of%20Legends-52x72.jpg",
            "template": "https://static-cdn.jtvnw.net/ttv-boxart/League%20of%20Legends-[width]x[height].jpg"
        ],
        "logo": [
            "large": "https://static-cdn.jtvnw.net/ttv-logoart/League%20of%20Legends-240x144.jpg",
            "medium": "https://static-cdn.jtvnw.net/ttv-logoart/League%20of%20Legends-120x72.jpg",
            "small": "https://static-cdn.jtvnw.net/ttv-logoart/League%20of%20Legends-60x36.jpg",
            "template": "https://static-cdn.jtvnw.net/ttv-logoart/League%20of%20Legends-[width]x[height].jpg"
        ],
        "localized_name": "League of Legends",
        "locale": ""
    ]
}
