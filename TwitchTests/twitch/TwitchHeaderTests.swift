import XCTest
import Alamofire
@testable import Twitch

class TwitchHeaderTests: XCTestCase {
    func testShouldValidateClientId() {
        let target = TwitchHeader.clientId.rawValue
        XCTAssertEqual(target, "wfl9ii3dy3lsdsr50t9rn4ms2shi6v")
    }

    func testShouldValidateToHeader() {
        let target = TwitchHeader.clientId.toHeader()
        XCTAssertEqual(target.key, "Client-ID")
        XCTAssertEqual(target.value, "wfl9ii3dy3lsdsr50t9rn4ms2shi6v")
    }
}
