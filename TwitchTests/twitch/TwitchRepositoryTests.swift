import XCTest
import GCDWebServers
@testable import Twitch

class TwitchRepositoryTests: XCTestCase {
    private func getWebServerUrl(for port: UInt) -> String {
        let webServer = GCDWebServer()!
        webServer.addDefaultHandler(forMethod: "GET", request: GCDWebServerRequest.self, processBlock: {request in
            return GCDWebServerDataResponse(jsonObject: Responses.validGamesJson)
        })
        webServer.start(withPort: port, bonjourName: "Twitch Web Server")

        return webServer.serverURL.absoluteString
    }

    func testShouldValidateGetTopGamesFromCache() {
        let paginator = Paginator(offset: 0, totalPages: 0, limit: 10)
        let store = InMemoryStore()
        store.save(games: Responses.validGamesJson)
        let target = TwitchRepository(client: TwitchClient(), store: store)

        target.getTopGames(paginator: paginator, success: { (games, _) in
            XCTAssertEqual(games.count, 1)
        }) { (_) in
            XCTFail("this closure should not be called for this test")
        }
    }

    func testShouldValidateGetTopGamesFromTwitchServer() {
        let client = TwitchClient(url: getWebServerUrl(for: 9000))
        let paginator = Paginator(offset: 0, totalPages: 0, limit: 10)
        let store = InMemoryStore()
        let target = TwitchRepository(client: client, store: store)

        let expectation = self.expectation(description: "Wait for asynchronous test")
        target.getTopGames(paginator: paginator, success: { (games, _) in
            XCTAssertEqual(games.count, 1)
            expectation.fulfill()
        }) { (_) in
            XCTFail("this closure should not be called for this test")
        }
        waitForExpectations(timeout: 3, handler: nil)
    }

    func testShouldValidateGetTopGamesFromTwitchServerPaginating() {
        let client = TwitchClient(url: getWebServerUrl(for: 9001))
        let paginator = Paginator(offset: 1, totalPages: 0, limit: 10)
        let store = InMemoryStore()
        let target = TwitchRepository(client: client, store: store)

        let expectation = self.expectation(description: "Wait for asynchronous test")
        target.getTopGames(paginator: paginator, success: { (games, _) in
            XCTAssertEqual(games.count, 1)
            expectation.fulfill()
        }) { (_) in
            XCTFail("this closure should not be called for this test")
        }
        waitForExpectations(timeout: 3, handler: nil)
    }

    func testShouldValidateFailureFlow() {
        let webServer = GCDWebServer()!
        webServer.addDefaultHandler(forMethod: "GET", request: GCDWebServerRequest.self, processBlock: {request in
            return GCDWebServerDataResponse(statusCode: 404)
        })
        webServer.start(withPort: 9002, bonjourName: "Twitch Web Server")

        let client = TwitchClient(url: webServer.serverURL.absoluteString)
        let store = InMemoryStore()
        let target = TwitchRepository(client: client, store: store)

        let expectation = self.expectation(description: "Wait for asynchronous test")
        target.getTopGames(paginator: .none, success: { (games, _) in
            XCTFail("this closure should not be called for this test")
        }) { (error) in
            expectation.fulfill()
            XCTAssertNotNil(error)
        }
        waitForExpectations(timeout: 3, handler: nil)
    }
}

fileprivate class InMemoryStore: Store {
    private var games: [String: Any] = [:]

    func save(games: [String: Any]?) {
        self.games = games!
    }

    func hasCache() -> Bool {
        return !games.isEmpty
    }

    func getTopGames(paginator: Paginator?, success: @escaping ([Game], Paginator?) -> (), failure: @escaping (Error?) -> ()) {
        guard let gamesDictionary = games["top"] as? [json] else {
            return failure(.none)
        }
        let paginator = paginator?.buildNext(totalItems: games["_total"] as? Int)
        success(gamesDictionary.flatMap{ Game(dict: $0) }, paginator)
    }
}
