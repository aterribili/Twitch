import XCTest
@testable import Twitch

class ImageSizeTests: XCTestCase {
    let target = ImageSize(dict: Responses.validJson)

    func testShouldValidateSmall() {
        XCTAssertEqual(target?.small,
                       .some("https://static-cdn.jtvnw.net/ttv-logoart/League%20of%20Legends-60x36.jpg"))
    }

    func testShouldValidateLarge() {
        XCTAssertEqual(target?.large, .some("https://static-cdn.jtvnw.net/ttv-logoart/League%20of%20Legends-240x144.jpg"))
    }

    func testShouldValidateMedium() {
        XCTAssertEqual(target?.medium, .some("https://static-cdn.jtvnw.net/ttv-logoart/League%20of%20Legends-120x72.jpg"))
    }

    func testWhenSmaillIsAbsentImageSizeShouldNotBeNil() {
        XCTAssertNotNil(target)
    }
}

extension ImageSizeTests {
    fileprivate struct Responses {
        static let validJson: [String: String] = [
            "large": "https://static-cdn.jtvnw.net/ttv-logoart/League%20of%20Legends-240x144.jpg",
            "medium": "https://static-cdn.jtvnw.net/ttv-logoart/League%20of%20Legends-120x72.jpg",
            "small": "https://static-cdn.jtvnw.net/ttv-logoart/League%20of%20Legends-60x36.jpg",
            "template": "https://static-cdn.jtvnw.net/ttv-logoart/League%20of%20Legends-[width]x[height].jpg"
        ]

        static let imageSizeJsonWithoutSmall: [String: String] = [
            "large": "https://static-cdn.jtvnw.net/ttv-logoart/League%20of%20Legends-240x144.jpg",
            "medium": "https://static-cdn.jtvnw.net/ttv-logoart/League%20of%20Legends-120x72.jpg",
            "template": "https://static-cdn.jtvnw.net/ttv-logoart/League%20of%20Legends-[width]x[height].jpg"
        ]
    }
}
