import XCTest
@testable import Twitch

class PaginatorTests: XCTestCase {
    func testWhenLimitMultipliedByOffsetIsGreaterThanTotalPageshasNextPageShouldBeTrue() {
        let target = Paginator(offset: 10, totalPages: 200, limit: 10)
        XCTAssertTrue(target.hasNextPage())
    }

    func testShouldValidateBuildNext() {
        let target = Paginator(offset: 10, totalPages: 100, limit: 10)
            .buildNext(totalItems: .none)

        XCTAssertFalse(target.hasNextPage())
        XCTAssertEqual(target.offset, 11)
        XCTAssertEqual(target.totalPages, 0)
    }

    func testShouldValidateDescription() {
        let target = Paginator(offset: 10, totalPages: 100, limit: 10)
        XCTAssertEqual(target.description, "?offset=10&limit=10")
    }
}
