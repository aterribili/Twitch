import XCTest
import GCDWebServers
@testable import Twitch

class ClientTests: XCTestCase {
    func testShouldValidateSuccessFlow() {
        let webServer = GCDWebServer()!
        webServer.addDefaultHandler(forMethod: "GET", request: GCDWebServerRequest.self, processBlock: {request in
            return GCDWebServerDataResponse(jsonObject: [:])
        })
        webServer.start(withPort: 7000, bonjourName: "Twitch Web Server")

        let expectation = self.expectation(description: "Wait for asynchronous test")

        _ = Client.get.request(url: webServer.serverURL, headers: [:], completion: { (result) in
            switch result {
            case .success(let value):
                XCTAssertNotNil(value)
            case .failure(_):
                XCTFail("this closure should not be called for this test")
            }
          expectation.fulfill()
        })
        waitForExpectations(timeout: 3, handler: nil)
    }

    func testShouldValidateFailureFlow() {
        let webServer = GCDWebServer()!
        webServer.addDefaultHandler(forMethod: "GET", request: GCDWebServerRequest.self, processBlock: {request in
            return GCDWebServerDataResponse(statusCode: 404)
        })
        webServer.start(withPort: 7001, bonjourName: "Twitch Web Server")

        let expectation = self.expectation(description: "Wait for asynchronous test")

        _ = Client.get.request(url: webServer.serverURL, headers: [:], completion: { (result) in
            switch result {
            case .success(_):
                XCTFail("this closure should not be called for this test")
            case .failure(let error):
                XCTAssertNotNil(error)
            }
            expectation.fulfill()
        })
        waitForExpectations(timeout: 3, handler: nil)
    }
}
