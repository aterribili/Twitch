
struct Paginator: CustomStringConvertible {
    let offset: Int
    let totalPages: Int
    let limit: Int

    init(offset: Int, totalPages: Int, limit: Int) {
        self.offset = offset
        self.totalPages = totalPages
        self.limit = limit
    }

    func hasNextPage() -> Bool {
        return offset < totalPages
    }

    func buildNext(totalItems: Int?) -> Paginator {
        let totalPages = ((totalItems ?? 0) / limit)
        return Paginator(offset: offset + 1, totalPages: totalPages, limit: limit)
    }

    var description: String {
        return "?offset=\(offset)&limit=\(limit)"
    }
}
