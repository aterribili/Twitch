import UIKit

extension UILabel {
    func applyDefaultStyle() {
        self.textColor = UIColor(red: 82/255, green: 73/255, blue: 71/255, alpha: 1)
        self.font = UIFont.systemFont(ofSize: 13, weight: UIFontWeightMedium)
    }
}
