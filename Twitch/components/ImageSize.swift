import Foundation

struct ImageSize {
    let small: String?
    let medium: String
    let large: String

    init?(dict: [String:String]?) {
        guard let dict = dict,
            let medium = dict["medium"],
            let large = dict["large"] else {
                return nil
        }

        self.small = dict["small"]
        self.medium = medium
        self.large = large
    }
}
