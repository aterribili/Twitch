import Alamofire

enum Client {
    case get

    private func toHTTPMethod() -> HTTPMethod {
        switch self {
        case .get:
            return HTTPMethod.get
        }
    }

    func request(url: URL, headers: [String: String], completion: @escaping (Result<Any>) -> ()) {
        self.toHTTPMethod().request(url: url, headers: headers, completion: completion)
    }
}

extension HTTPMethod {
    fileprivate func request(url: URL, headers: [String: String], completion: @escaping (Result<Any>) -> ()) {
        Alamofire.request(url,
                          method: self,
                          parameters: .none,
                          encoding: JSONEncoding.default,
                          headers: headers).responseJSON { (response) in
                            switch response.result {
                            case .success(let value):
                                return completion(Result.success(value))
                            case .failure(let error):
                                return completion(Result.failure(error))
                            }
        }
    }
}
