
protocol Header {
    func toHeader() -> (key: String, value: String)
}

extension Array where Element: Header {
    func toDict() -> [String: String] {
        return self
            .map { $0.toHeader() }
            .reduce([String:String]()) { (dict, tuple) in
                var nextDict = dict
                nextDict.updateValue(tuple.1, forKey: tuple.0)
                return nextDict
        }
    }
}
