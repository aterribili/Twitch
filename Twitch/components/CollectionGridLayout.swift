import UIKit

class CollectionGridLayout: UICollectionViewFlowLayout {
    override var itemSize: CGSize {
        set {}
        get {
            let numberOfColumns: CGFloat = 2

            let itemWidth = (self.collectionView!.frame.width - (numberOfColumns - 1)) / numberOfColumns
            return CGSize(width: itemWidth - 15, height: itemWidth+(itemWidth*0.08))
        }
    }
}
