import Foundation

struct Localizable {
    fileprivate enum LocalizableError: Error {
        case emptyKey
        case missingValue
    }

    static func localizableString(_ key: String) -> String {
        do {
            return try tryGetLocalizableString(key)
        } catch LocalizableError.emptyKey {
            return "LOCALIZED STRING ERROR - Empty key"
        } catch {
            return "LOCALIZED STRING ERROR - Missing value for key: \(key)"
        }
    }

    static func localizableCountableString(_ key: String, numberOfItems: Int) -> String {
        if numberOfItems > 1 {
            let newKey = "\(key).countable"
            return localizableString(newKey)
        }
        return localizableString(key)
    }

    fileprivate static func tryGetLocalizableString(_ key: String) throws -> String {
        #if TARGET_INTERFACE_BUILDER
            return "Debug IB"
        #endif

        if key.isEmpty {
            throw LocalizableError.emptyKey
        }

        let localizable = NSLocalizedString(key, comment: "Using Localizable Strings")

        if localizable == key {
            throw LocalizableError.missingValue
        }

        return localizable
    }
}
