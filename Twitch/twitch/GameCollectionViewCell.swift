import UIKit
import SnapKit
import SDWebImage

class GameCollectionViewCell: UICollectionViewCell {
    static let identifier = "GameTableViewCell"

    func setup(game: GameViewModel) {
        setupImage(game)
        setupName(game)
    }

    func setupImage(_ game: GameViewModel) {
        let gameImage = UIImageView()
        contentView.addSubview(gameImage)
        gameImage.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        gameImage.sd_setShowActivityIndicatorView(true)
        gameImage.sd_setIndicatorStyle(.gray)
        if let url = URL(string: game.getImageUrl()) {
            gameImage.sd_setImage(with: url, completed: { _ in
                self.layoutSubviews()
            })
        }
    }

    func setupName(_ game: GameViewModel) {
        let gameInformation = UIVisualEffectView(effect: UIBlurEffect(style: UIBlurEffectStyle.extraLight))
        contentView.addSubview(gameInformation)
        gameInformation.snp.makeConstraints { (make) in
            make.right.left.bottom.equalTo(self)
            make.height.equalTo(self.frame.height * 0.25)
        }

        let gameName = UILabel()
        gameInformation.addSubview(gameName)
        gameName.applyDefaultStyle()
        gameName.text = game.getName()
        gameName.snp.makeConstraints { (make) in
            make.top.left.right.equalToSuperview().inset(5)
        }
        gameName.sizeToFit()
        gameName.numberOfLines = 2
    }
}
