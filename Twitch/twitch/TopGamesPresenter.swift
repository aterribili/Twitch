
protocol TopGamesDelegate {
    func reloadData(games: [GameViewModel])
    func add(games: [GameViewModel])
    func loading()
    func loadingMore()
    func error(error: Error?)
}

class TopGamesPresenter {
    private let delegate: TopGamesDelegate
    private var paginator: Paginator?
    private let client = TwitchRepository()

    internal init(delegate: TopGamesDelegate){
        self.delegate = delegate
        self.paginator = Paginator(offset: 0, totalPages: 0, limit: 50)
    }

    internal func viewDidLoad() {
        delegate.loading()
        paginator = Paginator(offset: 0, totalPages: 0, limit: 50)
        client.getTopGames(paginator: paginator,
                                   success: { (games, paginator) in
                                    self.paginator = paginator
                                    self.delegate.reloadData(games: games.map{GameViewModel(game: $0)})
        }, failure: delegate.error)
    }

    internal func endReached() {
        if let paginator = self.paginator, paginator.hasNextPage(), client.hasInternetConnection() {
            delegate.loadingMore()

            client.getTopGames(paginator: paginator,
                               success: { (games, paginator) in
                                self.paginator = paginator
                                self.delegate.add(games: games.map{GameViewModel(game: $0)})
            }, failure: delegate.error)
        }
    }
}
