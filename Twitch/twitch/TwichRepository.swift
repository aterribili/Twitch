import Foundation
import Alamofire

struct TwitchRepository {
    private let client: TwitchClient
    private let store: Store

    init(client: TwitchClient = TwitchClient(), store: Store = GameStore()) {
        self.client = client
        self.store = store
    }

    func getTopGames(paginator: Paginator? = .none, success: @escaping ([Game], Paginator?) -> (), failure: @escaping (Error?) -> ()) {
        if paginator?.offset == 0 {
            if store.hasCache() {
                self.store.getTopGames(paginator: paginator, success: success, failure: failure)
            } else {
                client.getDict(success: { (dict) in
                    self.store.save(games: dict)
                    self.store.getTopGames(paginator: paginator, success: success, failure: failure)
                }, failure: failure)
            }
        } else {
            client.getTopGames(paginator: paginator, success: success, failure: failure)
        }
    }

    func hasInternetConnection() -> Bool {
        return NetworkReachabilityManager()?.isReachable ?? true
    }
}
