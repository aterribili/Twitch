import UIKit
import SnapKit
import SDWebImage

class GameDetailsViewController: UIViewController {
    private let viewModel: GameViewModel
    private let image = UIImageView()
    private let name = UILabel()
    private let viewers = UILabel()
    private let channels = UILabel()

    init(viewModel: GameViewModel) {
        self.viewModel = viewModel
        super.init(nibName: .none, bundle: .none)
        view.backgroundColor = UIColor.white
        self.title = viewModel.getName()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupImage()
        setupName()
        setupViewers()
        setupChannels()
    }

    private func setupImage() {
        view.addSubview(image)
        image.sd_setShowActivityIndicatorView(true)
        image.sd_setIndicatorStyle(.gray)
        if let url = URL(string: viewModel.getImageUrl()) {
            image.sd_setImage(with: url, completed: { _ in
                self.view.layoutSubviews()
            })
        }
        image.snp.makeConstraints { (make) in
            make.left.top.right.equalToSuperview().inset(UIEdgeInsetsMake(64, 0, 0, 0))
            make.height.equalTo(300)
        }
    }

    private func setupName() {
        view.addSubview(name)
        name.text = viewModel.getName()
        name.applyDefaultStyle()
        makeConstraints(view: name, base: image.snp.bottom)
    }

    private func setupViewers() {
        view.addSubview(viewers)
        viewers.text = viewModel.getViewers()
        viewers.applyDefaultStyle()
        makeConstraints(view: viewers, base: name.snp.bottom)
    }

    private func setupChannels() {
        view.addSubview(channels)
        channels.text = viewModel.getChannels()
        channels.applyDefaultStyle()
        makeConstraints(view: channels, base: viewers.snp.bottom)
    }

    private func makeConstraints(view: UIView, base: SnapKit.ConstraintItem) {
        view.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview().inset(10)
            make.top.equalTo(base).offset(10)
        }
    }
}
