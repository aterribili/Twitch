import Foundation

struct TwitchClient {
    private enum Urls: String {
        case topGames = "https://api.twitch.tv/kraken/games/top"
    }

    private let topGamesUrl: String

    init(url: String = Urls.topGames.rawValue) {
        self.topGamesUrl = url
    }

    func getTopGames(paginator: Paginator? = .none, success: @escaping ([Game], Paginator?) -> (), failure: @escaping (Error?) -> ()) {
        getDict(paginator: paginator, success: { (dict) in
            guard let gamesDict = dict["top"] as? [json] else {
                return failure(.none)
            }

            let games = gamesDict.flatMap{ Game(dict: $0) }
            let paginator = paginator?.buildNext(totalItems: dict["_total"] as? Int)
            success(games, paginator)
        }, failure: failure)
    }

    func getDict(paginator: Paginator? = .none, success: @escaping ([String: Any]) -> (), failure: @escaping (Error?) -> ()) {
        var stringUrl = self.topGamesUrl
        if let description = paginator?.description {
            stringUrl = "\(stringUrl)\(description)"
        }

        guard let url = URL(string: stringUrl) else {
            return failure(.none)
        }

        Client.get
            .request(url: url, headers: [TwitchHeader.clientId].toDict()) { (response) in
                switch response {
                case .success(let value):
                    guard let dict = value as? json else {
                        return failure(.none)
                    }

                    success(dict)
                case .failure(let error):
                    failure(error)
                }
        }
    }
}
