import Foundation

enum TwitchHeader: String, Header {
    case clientId = "wfl9ii3dy3lsdsr50t9rn4ms2shi6v"

    func toHeader() -> (key: String, value: String) {
        switch self {
        case .clientId:
            return (key: "Client-ID", value: self.rawValue)
        }
    }
}
