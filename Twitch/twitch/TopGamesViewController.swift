import UIKit
import SnapKit
import SDWebImage

class TopGamesViewController: UIViewController {
    fileprivate var presenter: TopGamesPresenter?
    fileprivate let indicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
    fileprivate let collectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: TopGamesViewController.gridLayout)
    fileprivate let refreshControl = UIRefreshControl()
    fileprivate var games: [GameViewModel] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = Localizable.localizableString("title")
        presenter = TopGamesPresenter(delegate: self)
        setupCollectionView()
        refresh()
    }

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.view.translatesAutoresizingMaskIntoConstraints = true
    }

    @objc private func refresh() {
        presenter?.viewDidLoad()
    }

    private func setupCollectionView() {
        collectionView.delegate = self
        collectionView.dataSource = self
        view.addSubview(collectionView)
        collectionView.backgroundColor = UIColor.white
        collectionView.register(GameCollectionViewCell.classForCoder(), forCellWithReuseIdentifier: GameCollectionViewCell.identifier)
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        collectionView.addSubview(refreshControl)
    }

    fileprivate func hideLoadingFeedback() {
        indicator.stopAnimating()
        indicator.removeFromSuperview()
        collectionView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        refreshControl.endRefreshing()
    }

    static var gridLayout: UICollectionViewLayout {
        let flowLayout = CollectionGridLayout()
        flowLayout.minimumInteritemSpacing = 1
        flowLayout.minimumLineSpacing = 10
        flowLayout.sectionInset = UIEdgeInsetsMake(10, 10, 0, 10)
        flowLayout.scrollDirection = .vertical

        return flowLayout
    }
}

extension TopGamesViewController: TopGamesDelegate {
    func error(error: Error?) {
        let alert = UIAlertController(title: Localizable.localizableString("title"), message: Localizable.localizableString("error.message"), preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: Localizable.localizableString("error.action.message"), style: .cancel, handler: { _ in
            self.viewDidLoad()
        }))
        self.present(alert, animated: true, completion: .none)
    }

    func reloadData(games: [GameViewModel]) {
        hideLoadingFeedback()
        self.games = games
        collectionView.reloadData()
    }

    func add(games: [GameViewModel]) {
        hideLoadingFeedback()
        self.games.append(contentsOf: games)
        collectionView.reloadData()
    }

    func loading() {
        view.addSubview(indicator)
        indicator.snp.makeConstraints { (make) in
            make.centerX.centerY.equalToSuperview()
        }
        indicator.startAnimating()
        view.bringSubview(toFront: indicator)
    }

    func loadingMore() {
        view.addSubview(indicator)
        indicator.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.bottom.equalTo(collectionView.snp.bottom).inset(20)
        }
        collectionView.snp.makeConstraints { (make) in
            make.left.top.right.equalToSuperview()
            make.bottom.equalTo(indicator.snp.top).offset(20)
        }
        indicator.startAnimating()
        view.bringSubview(toFront: indicator)
    }
}

extension TopGamesViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.item == (games.count - 1) {
            presenter?.endReached()
        }
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let game = games[indexPath.row]
        let detailsViewController = GameDetailsViewController(viewModel: game)
        self.navigationController?.pushViewController(detailsViewController, animated: true)
    }
}

extension TopGamesViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: GameCollectionViewCell.identifier,
                                                      for: indexPath) as! GameCollectionViewCell
        cell.setup(game: self.games[indexPath.row])
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return games.count
    }
}
