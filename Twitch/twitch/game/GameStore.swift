import Foundation

protocol Store {
    func save(games: [String: Any]?)
    func hasCache() -> Bool
    func getTopGames(paginator: Paginator?, success: @escaping ([Game], Paginator?) -> (), failure: @escaping (Error?) -> ())
}

struct GameStore: Store {
    private static let key = "games-response"

    func save(games: [String: Any]?) {
        UserDefaults.standard.set(games, forKey: GameStore.key)
    }

    func hasCache() -> Bool {
        return UserDefaults.standard.dictionary(forKey: GameStore.key)?.isEmpty ?? false
    }

    func getTopGames(paginator: Paginator? = .none, success: @escaping ([Game], Paginator?) -> (), failure: @escaping (Error?) -> ()) {
        guard let dict = UserDefaults.standard.dictionary(forKey: GameStore.key),
            let gamesDictionary = dict["top"] as? [json] else {
                return failure(.none)
        }
        let paginator = paginator?.buildNext(totalItems: dict["_total"] as? Int)
        success(gamesDictionary.flatMap{ Game(dict: $0) }, paginator)
    }
}
