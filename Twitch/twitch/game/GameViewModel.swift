import Foundation

struct GameViewModel {
    private let game: Game

    init(game: Game) {
        self.game = game
    }

    func getName() -> String {
        return game.details.name
    }

    func getImageUrl() -> String {
        return game.details.box.large
    }

    func getViewers() -> String {
        return "Viewers: \(self.game.viewers)"
    }

    func getChannels() -> String {
        return "Channels: \(self.game.channels)"
    }
}
