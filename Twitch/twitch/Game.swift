import Foundation

struct Game {
    let details: Details
    let viewers: Int
    let channels: Int

    init?(dict: json?) {
        guard let dict = dict,
            let viewers = dict["viewers"] as? Int,
            let channels = dict["channels"] as? Int,
            let details = Details(dict: dict["game"] as? json) else {
                return nil
        }

        self.viewers = viewers
        self.channels = channels
        self.details = details
    }
}
