import Foundation

extension Game {
    struct Details {
        let name: String
        let popularity: Int?
        let box: ImageSize
        let logo: ImageSize

        init?(dict: json?){
            guard let dict = dict,
                let name = dict["name"] as? String,
                let box = ImageSize(dict: dict["box"] as? [String:String]),
                let logo = ImageSize(dict: dict["logo"] as? [String:String]) else {
                    return nil
            }

            self.name = name
            self.popularity = dict["popularity"] as? Int
            self.box = box
            self.logo = logo
        }
    }
}
